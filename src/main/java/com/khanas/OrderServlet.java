package com.khanas;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/order")
public class OrderServlet extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<p><a href='ordered'>Ordered pizza</a></p>");
        out.println("<p><a href='delete'>Delete</a></p>");
        out.println("<p><a href='update'>Change order</a></p>");
        out.println("<h1>Order pizza</h1>");
        out.println("<h3>Type of pizza</h3>");
        out.println("<form action='ordered' method='Post'> \n"
                + "<select id='select-pizza' name='select-pizza' method='Post'> \n"
                + "<option value='Pepperoni'>Pepperoni</option> \n"
                + "<option value='Cheese'>Cheese</option> \n"
                + "<option value='Veggie'>Veggie</option> \n"
                + "<option value='Clam'>Clam</option> \n"
                + "</select> \n"
                + "<h3>Amount</h3>"
                + "<input type='number' min='1' name='count'> \n"
                + "<button type='submit'>Add order</button> \n"
                + "</form>");
        out.println("</body></html>");
    }

}
