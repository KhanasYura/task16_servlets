package com.khanas;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/delete")
public class DeleteServlet extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<p><a href='ordered'>Ordered pizza</a></p>");
        out.println("<p><a href='order'>Order</a></p>");
        out.println("<p><a href='update'>Change order</a></p>");
        out.println("<h1>Delete pizza</h1>");
        out.println("<h3>Number of order</h3>");
        out.println("<form action='ordered'> \n"
                + "<p> Pizza id: <input type='number' min='1' name='delete'> \n"
                + "<input type='button' onclick='remove(this.form.delete.value)'"
                + " name='button' value='Delete'> \n"
                + "</p>"
                + "</form>");
        out.println("<script type='text/javascript'>\n"
                + " function remove(id){"
                + "fetch('ordered/' + id,{method:'DELETE'});} \n"
                + "</script>");
        out.println("</body></html>");
    }

}

