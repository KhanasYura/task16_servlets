package com.khanas;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

@WebServlet("/update")
public class PutServlet extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<p><a href='ordered'>Ordered pizza</a></p>");
        out.println("<p><a href='order'>Order</a></p>");
        out.println("<p><a href='delete'>Delete</a></p>");
        out.println("<h1>Change order</h1>");
        out.println("<form action='' method='Post'>\n"
                + "Pizza id: <input type='number' min='1' name='id'>"
                + "<h3>Sauce</h3"
                + "<p><input type='radio' name='sauce' value='Pesto'>Pesto</p>\n"
                + "<p><input type='radio' name='sauce' value='Marinara'>Marinara</p>\n"
                + "<p><input type='radio' name='sauce' value='Tomato'>Tomato</p>\n"
                + "<p><input type='radio' name='sauce' value='Sour Cream'>Sour Cream</p>\n"
                + "<h3>Amount</h3>"
                + "<input type='number' min='1' name='amount'> \n"
                + "<h3>Radius</h3>"
                + "<select id='select-pizza' name='select-pizza'> \n"
                + "<option value='20'>20</option> \n"
                + "<option value='30'>30</option> \n"
                + "<option value='40'>40</option> \n"
                + "<option value='50'>50</option> \n"
                + "</select> \n"
                + "<h3>Toppings</h3"
                + "<p><input type='checkbox' name='toppings' value='Cheddar'>Cheddar</p>\n"
                + "<p><input type='checkbox' name='toppings' value='Parmesan'>Parmesan</p>\n"
                + "<p><input type='checkbox' name='toppings' value='Provolone'>Provolone</p>\n"
                + "<p><input type='checkbox' name='toppings' value='Mozzarella'>Mozzarella</p>\n"
                + "<p><input type='checkbox' name='toppings' value='Clams'>Clams</p>\n"
                + "<p><input type='checkbox' name='toppings' value='Oregano'>Oregano</p>\n"
                + "<p><input type='checkbox' name='toppings' value='Pork'>Pork</p>\n"
                + "<p><input type='checkbox' name='toppings' value='Sweet paprika'>Sweet paprika</p>\n"
                + "<p><input type='checkbox' name='toppings' value='Celery'>Celery</p>\n"
                + "<p><input type='checkbox' name='toppings' value='Cream Cheese'>Cream Cheese</p>\n"
                + "<p><input type='checkbox' name='toppings' value='Broccoli'>Broccoli</p>\n"
                + "<input type='submit' value='Send'>\n"
                + "</form>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        int count = Integer.parseInt(req.getParameter("select-pizza"));
        int amount = Integer.parseInt(req.getParameter("amount"));
        String sauce  = req.getParameter("sauce");
        int id = Integer.parseInt(req.getParameter("id"));
        String[] toppings = req.getParameterValues("toppings") ;
        PizzaServlet.pizzas.get(id).setRadius(count);
        PizzaServlet.pizzas.get(id).setAmount(amount);
        PizzaServlet.pizzas.get(id).setToppings(Arrays.asList(toppings));
        PizzaServlet.pizzas.get(id).setSauce(sauce);
    }

}

