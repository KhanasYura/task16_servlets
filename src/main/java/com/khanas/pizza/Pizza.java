package com.khanas.pizza;

import java.util.List;

public interface Pizza {
    String dough = "";
    String sauce = "";
    int radius = 0;
    int amount = 0;
    List<String> toppings = null;

    String info();

    int getAmount();

    void setAmount(int amount);

    String getDough();

    void setDough(final String dough);

    String getSauce();

    void setSauce(final String sauce);

    List<String> getToppings();

    void setToppings(final List<String> toppings);

    int getRadius();

    void setRadius(final int radius);
}
