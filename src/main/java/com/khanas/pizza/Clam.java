package com.khanas.pizza;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Clam implements Pizza {

    private String dough = "thick";
    private String sauce = "Marinara";
    private int radius = 30;
    private int amount;
    private List<String> toppings = new LinkedList<>(Arrays.asList("Clams",
            "Oregano", "Mozzarella"));

    public Clam(int amount) {
        this.amount = amount;
    }

    public final int getAmount() {
        return amount;
    }

    public final void setAmount(final int amount) {
        this.amount = amount;
    }

    public final String getDough() {
        return dough;
    }

    public final void setDough(final String dough) {
        this.dough = dough;
    }

    public final String getSauce() {
        return sauce;
    }

    public final void setSauce(final String sauce) {
        this.sauce = sauce;
    }

    public final List<String> getToppings() {
        return toppings;
    }

    public final void setToppings(final List<String> toppings) {
        this.toppings = toppings;
    }

    public final int getRadius() {
        return radius;
    }

    public final void setRadius(final int radius) {
        this.radius = radius;
    }

    public final String info() {
        return ("Clam{"
                + "radius" + radius + '\''
                + ", dough='" + dough + '\''
                + ", sauce='" + sauce + '\''
                + ", toppings=" + toppings
                + ", amount=" + amount + '}');
    }
}
