package com.khanas;


import com.khanas.pizza.Pepperoni;
import com.khanas.pizza.Veggie;
import com.khanas.pizza.Cheese;
import com.khanas.pizza.Clam;
import com.khanas.pizza.Pizza;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/ordered/*")
public class PizzaServlet extends HttpServlet {

    public static Map<Integer, Pizza> pizzas = new HashMap<>();

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<p><a href='order'>Order</a></p>");
        out.println("<p><a href='delete'>Delete</a></p>");
        out.println("<p><a href='update'>Change order</a></p>");
        out.println("<h1>Ordered pizza</h1>");
        if (pizzas.size() == 0) {
            out.println("<h3>There is no orders</h3>");
        } else {
            for (int pizza : pizzas.keySet()) {
                out.println("<p>" + pizza + " order: " + pizzas.get(pizza).info() + "</p>");
            }
        }
        out.println("</body></html>");
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws IOException {
        String name = req.getParameter("select-pizza");
        int count = Integer.parseInt(req.getParameter("count"));
        Pizza pizza = null;
        if (name.equals("Pepperoni")) {
            pizza = new Pepperoni(count);
        } else if (name.equals("Cheese")) {
            pizza = new Cheese(count);
        } else if (name.equals("Veggie")) {
            pizza = new Veggie(count);
        } else if (name.equals("Clam")) {
            pizza = new Clam(count);
        }
        pizzas.put(pizzas.size() + 1, pizza);
        doGet(req, resp);
    }

    @Override
    protected void doDelete(final HttpServletRequest req, final HttpServletResponse resp)
            throws IOException {
        String id = req.getRequestURI();
        id = id.replace("/RestAPI-1.0/ordered/", "");
        pizzas.remove(Integer.parseInt(id));
        doGet(req, resp);
    }

}
