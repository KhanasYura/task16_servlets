package com.khanas;

import com.khanas.pizza.Cheese;
import com.khanas.pizza.Pepperoni;
import com.khanas.pizza.Pizza;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/")
public class Menu extends HttpServlet {

    @Override
    public void init() {
        Pizza pizza = new Pepperoni(2);
        Pizza pizza1 = new Cheese(3);
        PizzaServlet.pizzas.put(1, pizza);
        PizzaServlet.pizzas.put(2, pizza1);
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<h1>Menu</h1>");
        out.println("<p><a href='ordered'>Ordered pizza</a></p>");
        out.println("<p><a href='order'>Order</a></p>");
        out.println("<p><a href='delete'>Delete</a></p>");
        out.println("<p><a href='update'>Change order</a></p>");
        out.println("</body></html>");
    }

}
